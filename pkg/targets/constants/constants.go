package constants

// Formats supported for generation
const (
	FormatCassandra                    = "cassandra"
	FormatClickhouse                   = "clickhouse"
	FormatClickhouseSingleTableNaive   = "clickhouse-single-table-naive"
	FormatClickhouseSingleTableRefined = "clickhouse-single-table-refined"
	FormatInflux                       = "influx"
	FormatMongo                        = "mongo"
	FormatSiriDB                       = "siridb"
	FormatTimescaleDB                  = "timescaledb"
	FormatAkumuli                      = "akumuli"
	FormatCrateDB                      = "cratedb"
	FormatPrometheus                   = "prometheus"
	FormatVictoriaMetrics              = "victoriametrics"
	FormatTimestream                   = "timestream"
	FormatQuestDB                      = "questdb"
)

func SupportedFormats() []string {
	return []string{
		FormatCassandra,
		FormatClickhouse,
		FormatClickhouseSingleTableNaive,
		FormatClickhouseSingleTableRefined,
		FormatInflux,
		FormatMongo,
		FormatSiriDB,
		FormatTimescaleDB,
		FormatAkumuli,
		FormatCrateDB,
		FormatPrometheus,
		FormatVictoriaMetrics,
		FormatTimestream,
		FormatQuestDB,
	}
}
