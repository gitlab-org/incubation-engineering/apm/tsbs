package clickhouse

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/timescale/tsbs/pkg/targets"
)

// NewNaiveBenchmark returns benchmark for naive ClickHouse table
func NewNaiveBenchmark(file string, hasWorkers bool, conf *ClickhouseConfig) targets.Benchmark {
	return &naiveBenchmark{
		benchmark: NewBenchmark(file, hasWorkers, conf).(*benchmark),
	}
}

// benchmark entrypoint for naive single table solution
type naiveBenchmark struct {
	*benchmark
}

func (b *naiveBenchmark) GetDBCreator() targets.DBCreator {
	return &naiveCreator{
		dbCreator: &dbCreator{ds: b.GetDataSource(), config: b.conf},
	}
}

func (b *naiveBenchmark) GetProcessor() targets.Processor {
	return &naiveProcessor{
		b.benchmark.GetProcessor().(*processor),
	}
}

// creator for naive single table solution
type naiveCreator struct {
	*dbCreator
}

func (c *naiveCreator) CreateDB(dbName string) error {
	// Connect to ClickHouse in general and CREATE DATABASE
	db := sqlx.MustConnect(dbType, getConnectString(c.config, false))
	sql := fmt.Sprintf("CREATE DATABASE %s", dbName)
	_, err := db.Exec(sql)
	if err != nil {
		panic(err)
	}
	db.Close()
	db = nil

	if tableCols == nil {
		tableCols = make(map[string][]string)
	}
	// init tableCols for later processing
	for tableName, fieldColumns := range c.headers.FieldKeys {
		tableCols[tableName] = fieldColumns
	}

	// Connect to specified database within ClickHouse
	db = sqlx.MustConnect(dbType, getConnectString(c.config, true))
	defer db.Close()

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS series
		(
			timestamp DateTime('UTC'),
			host String,
			measurement String,
			field String,
			value Float64,
			tags Array(String)
		) ENGINE = MergeTree()
		ORDER BY (host, measurement, field, timestamp)
	`)
	if err != nil {
		panic(err)
	}

	return nil
}

type naiveProcessor struct {
	*processor
}

func (p *naiveProcessor) Init(workerNum int, doLoad, hashWorkers bool) {
	p.processCSIFun = p.processCSI
	p.processor.Init(workerNum, doLoad, hashWorkers)
}

func (p *naiveProcessor) processCSI(measurement string, rows []*insertData) uint64 {
	ret := uint64(0)

	type seriesModel struct {
		Timestamp   time.Time
		Host        string
		Measurement string
		Field       string
		Value       float64
		Tags        []string
	}

	fields := tableCols[measurement]
	series := make([]seriesModel, 0, len(rows))

	for _, row := range rows {
		// tags line ex.:
		// hostname=host_0,region=eu-west-1,datacenter=eu-west-1b,rack=67,os=Ubuntu16.10,arch=x86,team=NYC,service=7,service_version=0,service_environment=production
		// assume hostname is the only tag that is static
		tags := strings.Split(row.tags, ",")
		host := strings.Split(tags[0], "=")[1]
		tags = tags[1:]

		// fields line ex.:
		// 1451606400000000000,58,2,24,61,22,63,6,44,80,38
		metrics := strings.Split(row.fields, ",")

		// Count number of metrics processed
		ret += uint64(len(metrics) - 1) // 1-st field is timestamp, do not count it
		// metrics = (
		// 	1451606400000000000,
		// 	58,
		// )

		// Build string TimeStamp as '2006-01-02 15:04:05.999999 -0700'
		// convert time from 1451606400000000000 (int64 UNIX TIMESTAMP with nanoseconds)
		timestampNano, err := strconv.ParseInt(metrics[0], 10, 64)
		if err != nil {
			panic(err)
		}
		timeUTC := time.Unix(0, timestampNano)
		metrics = metrics[1:]

		for i, f := range fields {
			f64, err := strconv.ParseFloat(metrics[i], 64)
			if err != nil {
				panic(err)
			}
			series = append(series, seriesModel{
				Timestamp:   timeUTC,
				Host:        host,
				Measurement: measurement,
				Field:       f,
				Value:       f64,
				Tags:        tags,
			})
		}
	}

	tx := p.db.MustBegin()
	stmt, err := tx.PrepareNamed(`INSERT INTO series (
		    timestamp, host, measurement, field, value, tags
		) VALUES (
			:timestamp, :host, :measurement, :field, :value, :tags
		))`)
	if err != nil {
		panic(err)
	}
	for _, s := range series {
		if _, err := stmt.Exec(s); err != nil {
			panic(err)
		}
	}
	err = stmt.Close()
	if err != nil {
		panic(err)
	}
	err = tx.Commit()
	if err != nil {
		panic(err)
	}

	return ret
}
