package clickhouse

import (
	"fmt"
	"strconv"
	"strings"
	"time"

	"github.com/jmoiron/sqlx"
	"github.com/timescale/tsbs/pkg/targets"
)

// NewRefinedBenchmark returns benchmark for refined ClickHouse table
func NewRefinedBenchmark(file string, hasWorkers bool, conf *ClickhouseConfig) targets.Benchmark {
	return &refinedBenchmark{
		benchmark: NewBenchmark(file, hasWorkers, conf).(*benchmark),
	}
}

// benchmark entrypoint for refined single table solution
type refinedBenchmark struct {
	*benchmark
}

func (b *refinedBenchmark) GetDBCreator() targets.DBCreator {
	return &refinedCreator{
		dbCreator: &dbCreator{ds: b.GetDataSource(), config: b.conf},
	}
}

func (b *refinedBenchmark) GetProcessor() targets.Processor {
	return &refinedProcessor{
		b.benchmark.GetProcessor().(*processor),
	}
}

// creator for refined single table solution
type refinedCreator struct {
	*dbCreator
}

func (c *refinedCreator) CreateDB(dbName string) error {
	// Connect to ClickHouse in general and CREATE DATABASE
	db := sqlx.MustConnect(dbType, getConnectString(c.config, false))
	sql := fmt.Sprintf("CREATE DATABASE %s", dbName)
	_, err := db.Exec(sql)
	if err != nil {
		panic(err)
	}
	db.Close()
	db = nil

	if tableCols == nil {
		tableCols = make(map[string][]string)
	}
	// init tableCols for later processing
	for tableName, fieldColumns := range c.headers.FieldKeys {
		tableCols[tableName] = fieldColumns
	}

	// Connect to specified database within ClickHouse
	db = sqlx.MustConnect(dbType, getConnectString(c.config, true))
	defer db.Close()

	_, err = db.Exec(`
		CREATE TABLE IF NOT EXISTS series
		(
			timestamp DateTime('UTC') CODEC(DoubleDelta, LZ4),
			host LowCardinality(String),
			measurement LowCardinality(String),
			` + "`fields.name`" + ` Array(LowCardinality(String)),
			` + "`fields.value`" + ` Array(Float64) CODEC(Gorilla, LZ4),
			tags Nested(
				key LowCardinality(String),
				value LowCardinality(String)
			)
		) ENGINE = MergeTree()
		PARTITION BY toYYYYMMDD(timestamp)
		ORDER BY (measurement, host, timestamp)
	`)
	if err != nil {
		panic(err)
	}

	return nil
}

type refinedProcessor struct {
	*processor
}

func (p *refinedProcessor) Init(workerNum int, doLoad, hashWorkers bool) {
	p.processCSIFun = p.processCSI
	p.processor.Init(workerNum, doLoad, hashWorkers)
}

func (p *refinedProcessor) processCSI(measurement string, rows []*insertData) uint64 {
	ret := uint64(0)

	type seriesModel struct {
		Timestamp   time.Time
		Host        string
		Measurement string
		FieldNames  []string  `db:"fields.name"`
		FieldValues []float64 `db:"fields.value"`
		TagKeys     []string  `db:"tags.key"`
		TagValues   []string  `db:"tags.value"`
	}

	fields := tableCols[measurement]
	series := make([]seriesModel, 0, len(rows))

	for _, row := range rows {
		// tags line ex.:
		// hostname=host_0,region=eu-west-1,datacenter=eu-west-1b,rack=67,os=Ubuntu16.10,arch=x86,team=NYC,service=7,service_version=0,service_environment=production
		// assume hostname is the only tag that is static
		tags := strings.Split(row.tags, ",")
		host := strings.Split(tags[0], "=")[1]
		tags = tags[1:]

		// fields line ex.:
		// 1451606400000000000,58,2,24,61,22,63,6,44,80,38
		metrics := strings.Split(row.fields, ",")

		// Count number of metrics processed
		ret += uint64(len(metrics) - 1) // 1-st field is timestamp, do not count it
		// metrics = (
		// 	1451606400000000000,
		// 	58,
		// )

		// Build string TimeStamp as '2006-01-02 15:04:05.999999 -0700'
		// convert time from 1451606400000000000 (int64 UNIX TIMESTAMP with nanoseconds)
		timestampNano, err := strconv.ParseInt(metrics[0], 10, 64)
		if err != nil {
			panic(err)
		}
		timeUTC := time.Unix(0, timestampNano)
		metrics = metrics[1:]

		tagKeys := make([]string, len(tags))
		tagValues := make([]string, len(tags))
		for i, t := range tags {
			s := strings.Split(t, "=")
			tagKeys[i] = s[0]
			tagValues[i] = s[1]
		}

		fieldNames := make([]string, len(fields))
		fieldValues := make([]float64, len(fields))
		for i, f := range fields {
			f64, err := strconv.ParseFloat(metrics[i], 64)
			if err != nil {
				panic(err)
			}
			fieldNames[i] = f
			fieldValues[i] = f64
		}

		series = append(series, seriesModel{
			Timestamp:   timeUTC,
			Host:        host,
			Measurement: measurement,
			FieldNames:  fieldNames,
			FieldValues: fieldValues,
			TagKeys:     tagKeys,
			TagValues:   tagValues,
		})
	}

	tx := p.db.MustBegin()
	stmt, err := tx.PrepareNamed(`INSERT INTO series (
		    timestamp, host, measurement, fields.name, fields.value, tags.key, tags.value
		) VALUES (
			:timestamp, :host, :measurement, :fields.name, :fields.value, :tags.key, :tags.value
		))`)
	if err != nil {
		panic(err)
	}
	for _, s := range series {
		if _, err := stmt.Exec(s); err != nil {
			panic(err)
		}
	}
	err = stmt.Close()
	if err != nil {
		panic(err)
	}
	err = tx.Commit()
	if err != nil {
		panic(err)
	}

	return ret
}
