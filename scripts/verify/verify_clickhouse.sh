#!/usr/bin/env bash

# verifies that data was successfully loaded into ClickHouse and metrics counts are correct
# assumes database is running in docker-compose with the correct service name

EXPECTED_ROWS=${EXPECTED_ROWS:-100}

clickhouse_verify_query() {
    rows=$(docker-compose exec -T clickhouse clickhouse-client -d benchmark -q "SELECT count(*) FROM ${1}")
    [ "$rows" -ne "$EXPECTED_ROWS" ] && { echo "$1 rows $rows does not match $EXPECTED_ROWS"; exit 1; }
}

clickhouse_verify_query "cpu"
clickhouse_verify_query "disk"
clickhouse_verify_query "diskio"
clickhouse_verify_query "kernel"
clickhouse_verify_query "mem"
clickhouse_verify_query "net"
clickhouse_verify_query "nginx"
clickhouse_verify_query "postgresl"
clickhouse_verify_query "redis"

echo "Verified each benchmark table has $EXPECTED_ROWS rows"