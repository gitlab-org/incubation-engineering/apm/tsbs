#!/usr/bin/env bash

# verifies that data was successfully loaded into timescaledb and metrics counts are correct
# assumes database is running in docker-compose with the correct service name

EXPECTED_ROWS=${EXPECTED_ROWS:-100}

timescale_verify_query() {
    rows=$(docker-compose exec -T timescaledb psql -U postgres -d benchmark -qtAX -c "SELECT count(*) FROM ${1};")
    [ "$rows" -ne "$EXPECTED_ROWS" ] && { echo "$1 rows $rows does not match $EXPECTED_ROWS"; exit 1; }
}

timescale_verify_query "cpu"
timescale_verify_query "disk"
timescale_verify_query "diskio"
timescale_verify_query "kernel"
timescale_verify_query "mem"
timescale_verify_query "net"
timescale_verify_query "nginx"
timescale_verify_query "postgresl"
timescale_verify_query "redis"

echo "Verified each benchmark relation has $EXPECTED_ROWS rows"