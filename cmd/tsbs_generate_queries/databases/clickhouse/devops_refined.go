package clickhouse

import (
	"fmt"
	"strings"
	"time"

	"github.com/timescale/tsbs/cmd/tsbs_generate_queries/uses/devops"
	"github.com/timescale/tsbs/cmd/tsbs_generate_queries/utils"
	"github.com/timescale/tsbs/pkg/query"
)

type BaseGeneratorRefined struct {
	*BaseGenerator
}

func (g *BaseGeneratorRefined) NewDevops(start, end time.Time, scale int) (utils.QueryGenerator, error) {
	core, err := devops.NewCore(start, end, scale)

	if err != nil {
		return nil, err
	}

	return &DevopsRefined{
		BaseGeneratorRefined: g,
		Core:                 core,
	}, nil
}

// DevopsRefined for refined table structure in ClickHouse
/*
CREATE TABLE IF NOT EXISTS series
(
	timestamp DateTime('UTC') CODEC(DoubleDelta, LZ4),
	host LowCardinality(String),
	measurement LowCardinality(String),
	`fields.name` Array(LowCardinality(String)),
	`fields.value` Array(Float64) CODEC(Gorilla, LZ4),
	tags Nested(
		key LowCardinality(String),
		value LowCardinality(String)
	)
) ENGINE = MergeTree()
PARTITION BY toYYYYMMDD(timestamp)
ORDER BY (measurement, host, timestamp)
*/
type DevopsRefined struct {
	*BaseGeneratorRefined
	*devops.Core
}

// MaxAllCPU selects the MAX of all metrics under 'cpu' per hour for nhosts hosts,
// e.g. in pseudo-SQL:
//
// SELECT field, max_statements
// FROM series
// WHERE
//		measurement = 'cpu'
// 		(host = '$HOSTNAME_1' OR ... OR host = '$HOSTNAME_N')
// 		AND time >= '$HOUR_START'
// 		AND time < '$HOUR_END'
// GROUP BY hour, field
// ORDER BY hour, field
//
// Resultsets:
// cpu-max-all-1
// cpu-max-all-8
func (d *DevopsRefined) MaxAllCPU(qi query.Query, nHosts int, duration time.Duration) {
	interval := d.Interval.MustRandWindow(duration)
	metrics := devops.GetAllCPUMetrics()

	sql := fmt.Sprintf(`
        SELECT
        	toStartOfHour(timestamp) AS hour,
        	%s
        FROM series
        WHERE (measurement = 'cpu') AND %s AND (timestamp >= '%s') AND (timestamp < '%s')
        GROUP BY hour
        ORDER BY hour
        `,
		d.aggregateFields("max", metrics),
		d.getHostWhereString(nHosts),
		interval.Start().Format(clickhouseTimeStringFormat),
		interval.End().Format(clickhouseTimeStringFormat))

	humanLabel := devops.GetMaxAllLabel("ClickHouse", nHosts)
	humanDesc := fmt.Sprintf("%s: %s", humanLabel, interval.StartString())
	d.fillInQuery(qi, humanLabel, humanDesc, devops.TableName, sql)
}

// GroupByTimeAndPrimaryTag selects the AVG of numMetrics metrics under 'cpu' per device per hour for a day,
// e.g. in pseudo-SQL:
//
// SELECT hour, host, AVG(metrics)
// FROM series
// WHERE measurement = 'cpu' AND time >= '$HOUR_START' AND time < '$HOUR_END'
// GROUP BY hour, host
// ORDER BY hour
//
// Resultsets:
// double-groupby-1
// double-groupby-5
// double-groupby-all
func (d *DevopsRefined) GroupByTimeAndPrimaryTag(qi query.Query, numMetrics int) {
	metrics, err := devops.GetCPUMetricsSlice(numMetrics)
	panicIfErr(err)
	interval := d.Interval.MustRandWindow(devops.DoubleGroupByDuration)

	sql := fmt.Sprintf(`
        SELECT
			toStartOfHour(timestamp) AS hour,
			host,
        	%s
        FROM series
		WHERE (measurement = 'cpu') AND (timestamp >= '%s') AND (timestamp < '%s')
		GROUP BY hour, host
        ORDER BY hour ASC, host
        `,
		d.aggregateFields("avg", metrics),
		interval.Start().Format(clickhouseTimeStringFormat), // cpu_avg time >= '%s'
		interval.End().Format(clickhouseTimeStringFormat),   // cpu_avg time < '%s'
	)

	humanLabel := devops.GetDoubleGroupByLabel("ClickHouse", numMetrics)
	humanDesc := fmt.Sprintf("%s: %s", humanLabel, interval.StartString())
	d.fillInQuery(qi, humanLabel, humanDesc, devops.TableName, sql)
}

// GroupByOrderByLimit populates a query.Query that has a time WHERE clause, that groups by a truncated date, orders by that date, and takes a limit:
// SELECT time_bucket('1 minute', time) AS t, MAX(value)
// FROM series
// WHERE measurement = 'cpu' AND time < '$TIME'
// GROUP BY t
// ORDER BY t DESC
// LIMIT $LIMIT
//
// Resultsets:
// groupby-orderby-limit
func (d *DevopsRefined) GroupByOrderByLimit(qi query.Query) {
	interval := d.Interval.MustRandWindow(time.Hour)

	sql := fmt.Sprintf(`
        SELECT
        	toStartOfMinute(timestamp) AS minute,
        	%s
        FROM series
        WHERE (measurement = 'cpu') AND timestamp < '%s'
        GROUP BY minute
        ORDER BY minute DESC
        LIMIT 5
        `,
		d.aggregateFields("max", []string{"usage_user"}),
		interval.End().Format(clickhouseTimeStringFormat))

	humanLabel := "ClickHouse max cpu over last 5 min-intervals (random end)"
	humanDesc := fmt.Sprintf("%s: %s", humanLabel, interval.EndString())
	d.fillInQuery(qi, humanLabel, humanDesc, devops.TableName, sql)
}

// HighCPUForHosts populates a query that gets CPU metrics when the CPU has high
// usage between a time period for a number of hosts (if 0, it will search all hosts),
// e.g. in pseudo-SQL:
//
// SELECT * FROM series
// WHERE measurement = 'cpu' AND usage_user > 90.0
// AND (host = '$HOST' OR host = '$HOST2'...)
// AND time >= '$TIME_START' AND time < '$TIME_END'
//
// Resultsets:
// high-cpu-1
// high-cpu-all
func (d *DevopsRefined) HighCPUForHosts(qi query.Query, nHosts int) {
	var hostWhereClause string
	if nHosts == 0 {
		hostWhereClause = ""
	} else {
		hostWhereClause = fmt.Sprintf("AND (%s)", d.getHostWhereString(nHosts))
	}
	interval := d.Interval.MustRandWindow(devops.HighCPUDuration)

	sql := fmt.Sprintf(`
		SELECT *
		FROM series
		WHERE (measurement = 'cpu') %s AND (fields.value[indexOf(fields.name, 'usage_user')] > 90.0) AND (timestamp >= '%s') AND (timestamp <  '%s')
        `,
		hostWhereClause,
		interval.Start().Format(clickhouseTimeStringFormat),
		interval.End().Format(clickhouseTimeStringFormat),
	)

	humanLabel, err := devops.GetHighCPULabel("ClickHouse", nHosts)
	panicIfErr(err)
	humanDesc := fmt.Sprintf("%s: %s", humanLabel, interval.StartString())
	d.fillInQuery(qi, humanLabel, humanDesc, devops.TableName, sql)
}

// LastPointPerHost finds the last row for every host in the dataset
//
// Resultsets:
// lastpoint
func (d *DevopsRefined) LastPointPerHost(qi query.Query) {
	sql := `
		SELECT *
		FROM series ANY INNER JOIN (
			SELECT max(timestamp) as timestamp, host
			FROM series
			WHERE measurement = 'cpu'
			GROUP BY host
		) as m USING host, timestamp
		WHERE measurement = 'cpu'
		ORDER BY host
	`

	humanLabel := "ClickHouse last row per host"
	humanDesc := humanLabel
	d.fillInQuery(qi, humanLabel, humanDesc, devops.TableName, sql)
}

// GroupByTime selects the MAX for numMetrics metrics under 'cpu' measurement,
// per minute for nhosts hosts,
// e.g. in pseudo-SQL:
//
// SELECT minute, max(metric1), ..., max(metricN)
// FROM series
// WHERE
//      (measurement = 'cpu') AND
// 		(hostname = '$HOSTNAME_1' OR ... OR hostname = '$HOSTNAME_N')
// 	AND time >= '$HOUR_START'
// 	AND time < '$HOUR_END'
// GROUP BY minute
// ORDER BY minute ASC
//
// Resultsets:
// single-groupby-1-1-12
// single-groupby-1-1-1
// single-groupby-1-8-1
// single-groupby-5-1-12
// single-groupby-5-1-1
// single-groupby-5-8-1
func (d *DevopsRefined) GroupByTime(qi query.Query, nHosts, numMetrics int, timeRange time.Duration) {
	interval := d.Interval.MustRandWindow(timeRange)
	metrics, err := devops.GetCPUMetricsSlice(numMetrics)
	panicIfErr(err)

	sql := fmt.Sprintf(`
        SELECT
        	toStartOfMinute(timestamp) AS minute,
			%s
        FROM series
        WHERE (measurement = 'cpu') AND %s AND (timestamp >= '%s') AND (timestamp < '%s')
        GROUP BY minute
        ORDER BY minute ASC
        `,
		d.aggregateFields("max", metrics),
		d.getHostWhereString(nHosts),
		interval.Start().Format(clickhouseTimeStringFormat),
		interval.End().Format(clickhouseTimeStringFormat))

	humanLabel := fmt.Sprintf("ClickHouse %d cpu metric(s), random %4d hosts, random %s by 1m", numMetrics, nHosts, timeRange)
	humanDesc := fmt.Sprintf("%s: %s", humanLabel, interval.StartString())
	d.fillInQuery(qi, humanLabel, humanDesc, devops.TableName, sql)
}

func (d *DevopsRefined) getHostWhereString(nhosts int) string {
	hostnames, err := d.GetRandomHosts(nhosts)
	panicIfErr(err)
	return d.getHostWhereWithHostnames(hostnames)
}

func (d *DevopsRefined) getHostWhereWithHostnames(hostnames []string) string {
	hostnameSelectionClauses := []string{}

	// All tags are included into one table
	// Need to prepare WHERE (hostname = 'host1' OR hostname = 'host2') clause
	for _, s := range hostnames {
		hostnameSelectionClauses = append(hostnameSelectionClauses, fmt.Sprintf("host = '%s'", s))
	}
	// (host=h1 OR host=h2)
	return "(" + strings.Join(hostnameSelectionClauses, " OR ") + ")"
}

func (d *DevopsRefined) getWhereInFields(fields []string) string {
	return fmt.Sprintf("field in ('%s')", strings.Join(fields, "', '"))
}

func (d *DevopsRefined) aggregateFields(aggregate string, fields []string) string {
	aggs := make([]string, len(fields))
	for i, f := range fields {
		aggs[i] = fmt.Sprintf("%s(fields.value[indexOf(fields.name, '%s')]) as %[1]s_%[2]s", aggregate, f)
	}

	return strings.Join(aggs, ", ")
}
