package clickhouse

import (
	"fmt"
	"strings"
	"time"

	"github.com/timescale/tsbs/cmd/tsbs_generate_queries/uses/devops"
	"github.com/timescale/tsbs/cmd/tsbs_generate_queries/utils"
	"github.com/timescale/tsbs/pkg/query"
)

type BaseGeneratorNaive struct {
	*BaseGenerator
}

func (g *BaseGeneratorNaive) NewDevops(start, end time.Time, scale int) (utils.QueryGenerator, error) {
	core, err := devops.NewCore(start, end, scale)

	if err != nil {
		return nil, err
	}

	return &DevopsNaive{
		BaseGeneratorNaive: g,
		Core:               core,
	}, nil
}

// DevopsNaive for naive table structure in ClickHouse
/*
CREATE TABLE IF NOT EXISTS series
(
	timestamp DateTime('UTC'),
	host String,
	measurement String,
	field String,
	value Float64,
	tags Array(String)
) ENGINE = MergeTree()
ORDER BY (host, measurement, field, timestamp)
*/
type DevopsNaive struct {
	*BaseGeneratorNaive
	*devops.Core
}

// MaxAllCPU selects the MAX of all metrics under 'cpu' per hour for nhosts hosts,
// e.g. in pseudo-SQL:
//
// SELECT field, MAX(value)
// FROM series
// WHERE
// 		(host = '$HOSTNAME_1' OR ... OR host = '$HOSTNAME_N')
//      AND measurement = 'cpu'
//      AND field in (cpu metrics...)
// 		AND time >= '$HOUR_START'
// 		AND time < '$HOUR_END'
// GROUP BY hour, field
// ORDER BY hour, field
//
// Resultsets:
// cpu-max-all-1
// cpu-max-all-8
func (d *DevopsNaive) MaxAllCPU(qi query.Query, nHosts int, duration time.Duration) {
	interval := d.Interval.MustRandWindow(duration)
	metrics := devops.GetAllCPUMetrics()

	sql := fmt.Sprintf(`
        SELECT
        	toStartOfHour(timestamp) AS hour,
        	field,
			max(value)
        FROM series
        WHERE %s AND (measurement = 'cpu') AND %s AND (timestamp >= '%s') AND (timestamp < '%s')
        GROUP BY hour, field
        ORDER BY hour, field
        `,
		d.getHostWhereString(nHosts),
		d.getWhereInFields(metrics),
		interval.Start().Format(clickhouseTimeStringFormat),
		interval.End().Format(clickhouseTimeStringFormat))

	humanLabel := devops.GetMaxAllLabel("ClickHouse", nHosts)
	humanDesc := fmt.Sprintf("%s: %s", humanLabel, interval.StartString())
	d.fillInQuery(qi, humanLabel, humanDesc, devops.TableName, sql)
}

// GroupByTimeAndPrimaryTag selects the AVG of numMetrics metrics under 'cpu' per device per hour for a day,
// e.g. in pseudo-SQL:
//
// SELECT field AVG(value)
// FROM series
// WHERE measurement = 'cpu' AND time >= '$HOUR_START' AND time < '$HOUR_END'
// GROUP BY hour, host, field
// ORDER BY hour
//
// Resultsets:
// double-groupby-1
// double-groupby-5
// double-groupby-all
func (d *DevopsNaive) GroupByTimeAndPrimaryTag(qi query.Query, numMetrics int) {
	metrics, err := devops.GetCPUMetricsSlice(numMetrics)
	panicIfErr(err)
	interval := d.Interval.MustRandWindow(devops.DoubleGroupByDuration)

	sql := fmt.Sprintf(`
        SELECT
			toStartOfHour(timestamp) AS hour,
			host,
        	field,
			avg(value)
        FROM series
		WHERE (measurement = 'cpu') AND %s AND (timestamp >= '%s') AND (timestamp < '%s')
		GROUP BY hour, host, field
        ORDER BY hour ASC, host
        `,
		d.getWhereInFields(metrics),                         // field in (metrics...)
		interval.Start().Format(clickhouseTimeStringFormat), // cpu_avg time >= '%s'
		interval.End().Format(clickhouseTimeStringFormat),   // cpu_avg time < '%s'
	)

	humanLabel := devops.GetDoubleGroupByLabel("ClickHouse", numMetrics)
	humanDesc := fmt.Sprintf("%s: %s", humanLabel, interval.StartString())
	d.fillInQuery(qi, humanLabel, humanDesc, devops.TableName, sql)
}

// GroupByOrderByLimit populates a query.Query that has a time WHERE clause, that groups by a truncated date, orders by that date, and takes a limit:
// SELECT time_bucket('1 minute', time) AS t, MAX(value)
// FROM series
// WHERE measurement = 'cpu' AND time < '$TIME'
// GROUP BY t
// ORDER BY t DESC
// LIMIT $LIMIT
//
// Resultsets:
// groupby-orderby-limit
func (d *DevopsNaive) GroupByOrderByLimit(qi query.Query) {
	interval := d.Interval.MustRandWindow(time.Hour)

	sql := fmt.Sprintf(`
        SELECT
        	toStartOfMinute(timestamp) AS minute,
        	max(value)
        FROM series
        WHERE (measurement = 'cpu') AND (field = 'usage_user') AND timestamp < '%s'
        GROUP BY minute
        ORDER BY minute DESC
        LIMIT 5
        `,
		interval.End().Format(clickhouseTimeStringFormat))

	humanLabel := "ClickHouse max cpu over last 5 min-intervals (random end)"
	humanDesc := fmt.Sprintf("%s: %s", humanLabel, interval.EndString())
	d.fillInQuery(qi, humanLabel, humanDesc, devops.TableName, sql)
}

// HighCPUForHosts populates a query that gets CPU metrics when the CPU has high
// usage between a time period for a number of hosts (if 0, it will search all hosts),
// e.g. in pseudo-SQL:
//
// SELECT * FROM series
// WHERE measurement = 'cpu' AND usage_user > 90.0
// AND (host = '$HOST' OR host = '$HOST2'...)
// AND time >= '$TIME_START' AND time < '$TIME_END'
//
// Resultsets:
// high-cpu-1
// high-cpu-all
func (d *DevopsNaive) HighCPUForHosts(qi query.Query, nHosts int) {
	var hostWhereClause string
	if nHosts == 0 {
		hostWhereClause = ""
	} else {
		hostWhereClause = fmt.Sprintf("AND (%s)", d.getHostWhereString(nHosts))
	}
	interval := d.Interval.MustRandWindow(devops.HighCPUDuration)

	sql := fmt.Sprintf(`
        SELECT timestamp, host, measurement, groupArray(tuple(field, value)) as fields
        FROM series
		WHERE (measurement = 'cpu') %s AND (timestamp >= '%s') AND (timestamp <  '%s')
		GROUP BY timestamp, host, measurement
		HAVING arrayExists(f -> f.1 = 'usage_user' AND f.2 > 90, fields)
		ORDER BY timestamp, host
        `,
		hostWhereClause,
		interval.Start().Format(clickhouseTimeStringFormat),
		interval.End().Format(clickhouseTimeStringFormat),
	)

	humanLabel, err := devops.GetHighCPULabel("ClickHouse", nHosts)
	panicIfErr(err)
	humanDesc := fmt.Sprintf("%s: %s", humanLabel, interval.StartString())
	d.fillInQuery(qi, humanLabel, humanDesc, devops.TableName, sql)
}

// LastPointPerHost finds the last row for every host in the dataset
//
// Resultsets:
// lastpoint
func (d *DevopsNaive) LastPointPerHost(qi query.Query) {
	sql := `
		SELECT timestamp, host, measurement, groupArray(tuple(field, value)) as fields
		FROM series INNER JOIN (
			SELECT max(timestamp) as timestamp, host
			FROM series
			WHERE measurement = 'cpu'
			GROUP BY host
		) AS hosts ON series.timestamp = hosts.timestamp AND series.host = hosts.host
		WHERE measurement = 'cpu'
		GROUP BY timestamp, host, measurement
		ORDER BY timestamp, host
	`

	humanLabel := "ClickHouse last row per host"
	humanDesc := humanLabel
	d.fillInQuery(qi, humanLabel, humanDesc, devops.TableName, sql)
}

// GroupByTime selects the MAX for numMetrics metrics under 'cpu',
// per minute for nhosts hosts,
// e.g. in pseudo-SQL:
//
// SELECT minute, max(metric1), ..., max(metricN)
// FROM series
// WHERE
//      (measurement = 'cpu') AND
// 		(hostname = '$HOSTNAME_1' OR ... OR hostname = '$HOSTNAME_N')
// 	AND time >= '$HOUR_START'
// 	AND time < '$HOUR_END'
// GROUP BY minute
// ORDER BY minute ASC
//
// Resultsets:
// single-groupby-1-1-12
// single-groupby-1-1-1
// single-groupby-1-8-1
// single-groupby-5-1-12
// single-groupby-5-1-1
// single-groupby-5-8-1
func (d *DevopsNaive) GroupByTime(qi query.Query, nHosts, numMetrics int, timeRange time.Duration) {
	interval := d.Interval.MustRandWindow(timeRange)
	metrics, err := devops.GetCPUMetricsSlice(numMetrics)
	panicIfErr(err)

	sql := fmt.Sprintf(`
        SELECT
        	toStartOfMinute(timestamp) AS minute,
			field,
        	max(value)
        FROM series
        WHERE %s AND (measurement = 'cpu') AND %s AND (timestamp >= '%s') AND (timestamp < '%s')
        GROUP BY minute, field
        ORDER BY minute ASC, field
        `,
		d.getHostWhereString(nHosts),
		d.getWhereInFields(metrics),
		interval.Start().Format(clickhouseTimeStringFormat),
		interval.End().Format(clickhouseTimeStringFormat))

	humanLabel := fmt.Sprintf("ClickHouse %d cpu metric(s), random %4d hosts, random %s by 1m", numMetrics, nHosts, timeRange)
	humanDesc := fmt.Sprintf("%s: %s", humanLabel, interval.StartString())
	d.fillInQuery(qi, humanLabel, humanDesc, devops.TableName, sql)
}

func (d *DevopsNaive) getHostWhereString(nhosts int) string {
	hostnames, err := d.GetRandomHosts(nhosts)
	panicIfErr(err)
	return d.getHostWhereWithHostnames(hostnames)
}

func (d *DevopsNaive) getHostWhereWithHostnames(hostnames []string) string {
	hostnameSelectionClauses := []string{}

	// All tags are included into one table
	// Need to prepare WHERE (hostname = 'host1' OR hostname = 'host2') clause
	for _, s := range hostnames {
		hostnameSelectionClauses = append(hostnameSelectionClauses, fmt.Sprintf("host = '%s'", s))
	}
	// (host=h1 OR host=h2)
	return "(" + strings.Join(hostnameSelectionClauses, " OR ") + ")"
}

func (d *DevopsNaive) getWhereInFields(fields []string) string {
	return fmt.Sprintf("field in ('%s')", strings.Join(fields, "', '"))
}
