#!/usr/bin/env bash

# compare ClickHouse single table approaches for devops workloads

# fixed with 4 workers

export DATABASE_NAMES="timescaledb clickhouse clickhouse-single-table-naive clickhouse-single-table-refined"
export NUM_WORKERS=4

# ensure we're starting from scratch, ensure volumes deleted
docker-compose down -v

# ensure runner is built fresh
docker-compose build runner

# ensure logs are saved and that we own the files, then erase volumes
clean_up() {
    sudo chown -R "$USER:$USER" results
    docker-compose logs -t --no-log-prefix runner > results/"$1"
    docker-compose down -v
}

# devops, 10 nodes
SCALE=10 USE_CASE=devops docker-compose up runner
clean_up "logs_runner_10_devops.txt"

# devops, 100 nodes
SCALE=100 USE_CASE=devops docker-compose up runner
clean_up "logs_runner_100_devops.txt"

# devops, 1000 nodes
SCALE=1000 USE_CASE=devops docker-compose up runner
clean_up "logs_runner_1000_devops.txt"