#!/usr/bin/env bash

# compare ClickHouse and TimescaleDB for devops workloads

# fixed with 4 workers

export DATABASE_NAMES="timescaledb clickhouse"
export NUM_WORKERS=4

# ensure we're starting from scratch, ensure volumes deleted
docker-compose down -v

# ensure runner is built fresh
docker-compose build runner

# ensure logs are saved and that we own the files, then erase volumes
clean_up() {
    sudo chown -R "$USER:$USER" results
    docker-compose logs -t --no-log-prefix runner > results/"$1"
    docker-compose down -v
}

# cpu-only, 100 nodes
SCALE=100 USE_CASE=cpu-only docker-compose up runner
clean_up "logs_runner_100_cpu-only.txt"

# devops full case, 100 nodes
SCALE=100 USE_CASE=devops docker-compose up runner
clean_up "logs_runner_100_devops.txt"

# cpu-only, 1000 nodes
SCALE=1000 USE_CASE=cpu-only docker-compose up runner
clean_up "logs_runner_1000_cpu-only.txt"

# devops full case, 1000 nodes
SCALE=1000 USE_CASE=devops docker-compose up runner
clean_up "logs_runner_1000_devops.txt"

# cpu-only, 4000 nodes
SCALE=4000 USE_CASE=cpu-only docker-compose up runner
clean_up "logs_runner_4000_cpu-only.txt"

# devops full case, 4000 nodes
SCALE=4000 USE_CASE=devops docker-compose up runner
clean_up "logs_runner_4000_devops.txt"