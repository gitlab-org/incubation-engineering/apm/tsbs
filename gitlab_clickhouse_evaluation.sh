#!/usr/bin/env bash

# runs benchmarks against clickhouse schemas in development and against a few other selected dbs.

START_TIME="$(date -Iseconds)"

# 0 seed uses current timestamp for data generation
export SEED="${SEED:-0}"
# devops use case is best match for our work
export USE_CASE="${USE_CASE:-devops}"
# 100 nodes, lots of devops data per node is generated
export SCALE="${SCALE:-100}"
# 2592000 expected rows in timescaledb for devops usage and scale=100
export EXPECTED_ROWS="${EXPECTED_ROWS:-2592000}"

# results directory
export RESULTS_DIR="${RESULTS_DIR:-$PWD/results/${USE_CASE}_${SCALE}_${START_TIME}}"

DATABASE_NAMES=${DATABASE_NAMES:-"timescaledb clickhouse"}

# amount of time to sleep for between loading and querying
LOAD_SETTLE=${LOAD_SETTLE:-"30"}

log() {
    echo "~ $(date -R) $1"
}

prom_query() {
    # query all container cpu and memory metrics for a container over a range
    curl -sG --data-urlencode "query={__name__=~\"container_(cpu|memory).*\",name=\"$1\"}" --data-urlencode "start=$2" --data-urlencode "end=$3" --data-urlencode "step=10s" "http://localhost:9090/api/v1/query_range"
}

mkdir -p "$RESULTS_DIR"

# ensure existing databases are stopped
docker-compose stop $DATABASE_NAMES

# start prometheus and cadvisor to perform monitoring of the databases
log "Starting cadvisor and prometheus"
docker-compose up -d cadvisor prometheus

# database test cycles
for DB_NAME in ${DATABASE_NAMES}; do
    log "Generating ${USE_CASE} data for ${DB_NAME}"
    FORMATS="${DB_NAME}" EXE_FILE_NAME="$(pwd)/bin/tsbs_generate_data" ./scripts/generate_data.sh

    log "Starting ${DB_NAME}"
    docker-compose up -d --force-recreate "${DB_NAME}"

    # start time for querying prometheus
    db_start_time="$(date +%s)"

    log "Waiting for ${DB_NAME} to start"
    sleep 5

    log "Generating ${DB_NAME} queries"
    FORMATS="${DB_NAME}" EXE_FILE_NAME="$(pwd)/bin/tsbs_generate_queries" ./scripts/generate_queries.sh

    log "Loading ${DB_NAME} with data"
    EXE_FILE_NAME="$(pwd)/bin/tsbs_load_${DB_NAME}" RESULTS_FILE="${RESULTS_DIR}/${DB_NAME}_load_results_${START_TIME}.json" ./scripts/load/load_${DB_NAME}.sh

    log "Verifying ${DB_NAME} data"
    ./scripts/verify/verify_${DB_NAME}.sh

    log "Wait for ${DB_NAME} to settle"
    sleep "$LOAD_SETTLE"

    log "Running queries ${DB_NAME}"
    EXE_FILE_NAME="$(pwd)/bin/tsbs_run_queries_${DB_NAME}" ./scripts/run_queries/run_queries_${DB_NAME}.sh "/tmp/bulk_queries/${DB_NAME}"-*-queries.gz

    log "Stopping ${DB_NAME}"
    docker-compose stop "${DB_NAME}"

    db_stop_time="$(date +%s)"

    log "Storing logs for ${DB_NAME}"
    docker-compose logs -t --no-log-prefix "${DB_NAME}" > "${RESULTS_DIR}/${DB_NAME}_container_logs_${START_TIME}.txt"

    log "Collecting volume sizes for ${DB_NAME}"
    volume_ids="$(docker inspect -f '{{ range $key, $val := .Mounts }}{{ $val.Name }} {{ end }}' "${DB_NAME}")"
    echo "${volume_ids}" | xargs docker volume inspect -f '{{ .Mountpoint }}' | xargs du -s > "${RESULTS_DIR}/${DB_NAME}_volumes_size_${START_TIME}.txt"

    log "Querying prometheus data for ${DB_NAME}"
    prom_query "${DB_NAME}" "$db_start_time" "$db_stop_time" > "${RESULTS_DIR}/${DB_NAME}_prom_container_metrics.json"
done