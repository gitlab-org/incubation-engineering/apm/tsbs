# Build stage
FROM golang:1.16-alpine AS builder
RUN apk update && apk add --no-cache git make
WORKDIR /tsbs
COPY cmd ./cmd
COPY internal ./internal
COPY pkg ./pkg
COPY load ./load
COPY go.mod go.sum ./
COPY Makefile ./
RUN make all

# Final stage
FROM alpine:latest
RUN apk update && apk add --no-cache bash docker docker-compose curl
WORKDIR /tsbs
COPY --from=builder /tsbs/bin ./bin
COPY ./scripts ./scripts
COPY ./docker-compose.yml ./
COPY ./gitlab_clickhouse_evaluation.sh ./
ENTRYPOINT ["/tsbs/gitlab_clickhouse_evaluation.sh"]
